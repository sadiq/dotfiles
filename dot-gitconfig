# -*- mode: conf; -*-

# doc: man 1 git config
#    : https://git-scm.com/docs/git-config

[init]
    defaultBranch = main

[core]
    editor = emacsclient -a= -c
    pager = delta
    excludesfile = ~/.gitignore

[alias]
    # Checkout the given branch, or show the list of branches if branch not specified
    b    = "!f() { : git branch ; if [ "$1" ]; then git checkout -b $1;\
            else git branch --sort=committerdate --color | tail -n 30; fi; }; f"
    ca   = commit --amend
    cm   = "!f() { if [ \"$(git branch -a --format \"%(refname:lstrip=3)\" | grep \"^main$\")\" ];\
            then git checkout main; else git checkout master; fi;}; f"
    # Checkout specified banch, or to the last switched branch if none specified
    c   = "!f() { : git checkout ; if [ \"$1\" ]; then git checkout $1;\
            else git checkout @{-1}; fi;}; f"
    d    = diff --no-prefix
    # Add a "fixup!" commit after adding specified/all modified files
    # fixme: $@ isn't working here
    f    = "!f() { if [ ! \"$(git diff --cached)\" ]; then git add -u; fi;\
            git commit -m \"fixup! $1 $2 $3 $4\"; }; f"
    lo   = log --oneline
    last = show HEAD
    m    = commit -m
    r    = "!f() { if [ "$(git rev-parse --verify main 2>/dev/null)" ];\
            then git rebase main; else git rebase master; fi;}; f"
    # 0 is prefixed so as to not go astray when $1 or HEAD is missing
    rh   = "!f() { if [ "0$1" -ge 1 ]; then LIMIT=$1; else LIMIT=30; fi;\
            if [ "0$(git rev-list --count HEAD 2>/dev/null)" -gt $LIMIT ];\
            then git rebase -i HEAD~$LIMIT; else git rebase -i --root; fi;}; f"
    s    = status -sb

[color]
    ui = auto

[format]
    pretty = oneline

[credential]
    helper = cache

[interactive]
    diffFilter = delta --color-only

[merge]
    conflictstyle = diff3

[diff]
    colorMoved = default

[delta]
    navigate = true
    light = false
    file-decoration-style = omit
    file-style = bold "33"
    hunk-header-decoration-style = "237" ul
    hunk-header-line-number-style = "33"

# git silently ignores missing config files.
# So we can simply remove the file to delete the config
[include]
    path = ~/.gitconfig.sadiq
    path = ~/.gitconfig.personal

# Functions used in bashrc and elsewhere

gcck3()
{
  gcc "$@" -o "${1%.c}" $(pkg-config --cflags --libs gtk+-3.0 2>/dev/null) -Wall -g -O0 \
      -Wno-unused-parameter -fno-omit-frame-pointer && echo "complied output: ${1%.c}"
}

gcck4()
{
  gcc "$@" -o "${1%.c}" $(pkg-config --cflags --libs gtk4 2>/dev/null) -Wall -g -O0 \
      -Wno-unused-parameter -fno-omit-frame-pointer && echo "complied output: ${1%.c}"
}


# Using '.' as separator so that I can stay lazy by not moving my fingers to reach '_'
my.debug()
{
  export G_DEBUG="gc-friendly"
  # todo: Remove after glib 2.76 is in Debian
  # See https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2935
  export G_SLICE="debug-blocks"
}

my.is_mobile()
{
  if [[ ! -e "/etc/os-release" ]]; then
    if type termux-setup-storage >/dev/null 2>&1; then
      return $(true)
    fi

    return $(false)
  fi

  . /etc/os-release

  if [[ "$ID" = "postmarketos" ]]; then
    return $(true)
  elif [[ "$ID" = "debian" || "$ID_LIKE" = "debian" ]]; then
    if [[ "$(id -u -n)" = "purism" || "$(id -u -n)" = "mobian" ]]; then
      return $(true)
    fi
  fi

  return $(false)
}

my.prompt_command()
{
  if [[ ! "$(type -P who 2>/dev/null)" ]]; then
    return
  fi

  # note: Alpine requires utmps package
  MY_SSH_COUNT="$(who | grep -cE ' \(([0-9]*\.){3}[0-9]*\)| \(.*:.*:.*\)')"

  # Don't count self for remote logins
  [[ "${SSH_CLIENT}" ]] && ((MY_SSH_COUNT--))

  if [[ "$MY_SSH_COUNT" -ge 1 ]]; then
    MY_SSH_PS1="$MY_SSH_COUNT:"
  else
    MY_SSH_PS1=""
  fi
}

my.update_ps1()
{
  # Don't do anything fancy if dumb
  if [[ $TERM == dumb ]]; then
    PS1="$ "
    return
  fi

  # trim directory if we are too deep, it's just a `pwd` away
  export PROMPT_DIRTRIM=4

  PS1=""

  # If the file ~/.bash/.default exists, behave the same as the default PS1
  if [[ -f ~/.bash/.default ]]; then
    PS1="\[${PS_BOLD}${PS_GREEN}\]\u"
  else
    # Show username and hostname once
    echo -e "${PS_BOLD}${PS_GREEN}$(id -u -n)@$(hostname)${PS_RESET}"
  fi

  # @host:
  if [[ "$SSH_CLIENT" || "$SSH_TTY" || -f ~/.bash/.default ]]; then
    if [[ "$SSH_CLIENT" || "$SSH_TTY" ]]; then
      COLOR=${PS_LRED}
    else
      COLOR=${PS_GREEN}
    fi
    PS1="$PS1\[${PS_BOLD}${COLOR}\]@\[${PS_GREEN}\]\h\[${COLOR}\]:"
  fi

  # Show count of ssh users if any
  PS1="\[${PS_BOLD}${PS_LRED}\]"'$MY_SSH_PS1'"$PS1"

  # directory path
  if [[ "$1" ]]; then
    PS1="$PS1\[${PS_BLUE}\]$1"
  else
    PS1="$PS1\[${PS_BLUE}\]\w"
  fi

  # If user is `root' use '#', else use '>'
  if [[ "$UID" -eq 0 ]]; then
    PS1="$PS1\[${PS_RED}\]#\[${PS_RESET}\] "
  elif [[ -f ~/.bash/.default ]]; then
    PS1="$PS1\[${PS_PINK}\]$\[${PS_RESET}\] "
  else
    PS1="$PS1\[${PS_PINK}\]>\[${PS_RESET}\] "
  fi

  # Set default title
  PS1="$PS1\[\e]2;\a\]"
}

my.update_ps1.small()
{
  my.update_ps1 "\W"
}

my.docker.run()
{
  IMAGE="$1"
  CMD="$2"

  if [[ ! "$IMAGE" ]]; then
    IMAGE=debian
  fi

  if [[ ! "$CMD" ]]; then
    CMD=bash
  fi

  sudo docker run -v $(pwd):/root/work -w /root/work -it "$IMAGE" "$CMD"
}

my.podman.run()
{
  IMAGE="$1"
  CMD="$2"

  if [[ ! "$IMAGE" ]]; then
    IMAGE=debian
  fi

  if [[ ! "$CMD" ]]; then
    CMD=bash
  fi

  podman run  -v $(pwd):/root/work -w /root/work -it "$IMAGE" "$CMD"
}

my.bst.open()
{
  local PROJECT="$1"
  local PROJECT_PATH="$2"
  local MATCH
  local ARGS=""

  if [[ ! "$PROJECT" ]]; then
    if [[ $(pwd) == "$HOME/Projects/"* ]]; then
      PROJECT=$(basename $(pwd | grep "$HOME/Projects/[^/]*" -o))
    else
      echo 'Provide a project name'
      return
    fi
  fi

  if [[ ! "$PROJECT_PATH" ]]; then
    PROJECT_PATH="$HOME/Projects/$PROJECT"
  fi

  MATCH=$(cd $HOME/Projects/gnome-build-meta/elements; find * -name "$PROJECT.bst")

  if [[ ! "$MATCH" ]]; then
    echo "$PROJECT.bst not found in '$HOME/Projects/gnome-build-meta/'"
    return
  fi

  if [[ "$(ls $PROJECT_PATH/)" ]]; then
    ARGS="--no-checkout"
  fi

  (cd $HOME/Projects/gnome-build-meta/elements;
   bst workspace open "$ARGS" --directory $PROJECT_PATH $MATCH)
}

# If not running interactively, don't do anything
[[ $- == *i* ]] || return

# History related settings
shopt -s histappend               # Append to history file
HISTCONTROL=ignoreboth:erasedups  # Ignore commands that begin with space/are duplicate
HISTSIZE=5000                     # Maximum commands to save in history file
HISTFILESIZE=10000                # Maximum number of lines to save

# Autocorrect cd typos
shopt -s cdspell

# Check width of term and adjust (if window size changes) after each command.
shopt -s checkwinsize

source ~/.bash/colors.sh
source ~/.bash/functions.sh
source ~/.bash/custom-env.sh
source ~/.bash/aliases.sh

PROMPT_COMMAND=my.prompt_command
my.prompt_command

# Update PS1
if my.is_mobile || [ "$INSIDE_EMACS" ] || [ "$INSIDE_GNOME_BUILDER" ]; then
  my.update_ps1.small
else
  my.update_ps1
fi

# Be debug friendly by default in GNU Emacs and GNOME Builder
if [ "$INSIDE_EMACS" ] || [ "$INSIDE_GNOME_BUILDER" ]; then
  my.debug
fi

# Enable bash completion
if [[ -f /usr/share/bash-completion/bash_completion ]]; then
  source /usr/share/bash-completion/bash_completion
elif [[ -f /etc/bash_completion ]]; then
  source /etc/bash_completion
fi

# Some nice to have variables
export LESS='-XFRi'
export EDITOR='emacs'
export VISUAL='emacs'

export CC_LD=mold
export MALLOC_PERTURB_="$((RANDOM % 255 + 1))"
export MALLOC_CHECK_=3

PATH=~/.local/bin:~/.cargo/bin:$PATH
export PATH="/usr/lib/ccache:$PATH"

# Enforce x11 for qt applications when run over ssh
if [[ "$SSH_CLIENT" ]]; then
  export QT_QPA_PLATFORM=xcb;
fi

# local configuration
if [[ -f ~/.bash/sadiq.sh ]]; then
  source ~/.bash/sadiq.sh
fi

# personal scripts and confs
# This is placed last so as to override anything above
if [[ -f ~/.bash/personal.sh ]]; then
  source ~/.bash/personal.sh
fi

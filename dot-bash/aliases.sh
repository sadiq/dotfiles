# Common aliases to switch directories
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
# The command is `-'. `--' here means, arguments for `alias' ends here.
# What follows `--' won't be taken as an argument for `alias',
# and will be taken literally
alias -- -='cd -'

alias ls='ls --color=auto -BhGFSr'
alias ll='ls --color=auto -BhFlA'

alias grep='grep --color=auto'
alias grepr='grep --exclude-dir=.git --exclude-dir=.ccls-cache --exclude-dir=_build -R'
alias grepc='grep --include=*.c --include=*.h --exclude-dir=.ccls-cache -R'
alias grepui='grep --include=*.ui -R'

alias free='free -h'
alias df='df -hT'
alias du='du -hc'
alias so='sudo swapoff -a && sudo swapon -a'
alias cpv='rsync -ah --info=progress2'
alias sudo='sudo SSH_CLIENT="${SSH_CLIENT}"'
alias ip='ip --color=auto'
alias c='batcat'
alias e='emacsclient -a= -c'
alias t='e ~/todo/todo.org'
alias ts='ts -s "%.s"' # Show incremental timestamp

# If gtypist.typ file exists in $HOME, open that
export GTYPIST_PATH=$HOME
alias gtypist='gtypist --banner-colors=7,0,0,0 gtypist.typ'

# Various cd shortcuts
alias cdm="cd '/media/sadiq/main'"

if [[ "$(type -P colordiff 2>/dev/null)" ]]
then
  alias diff=colordiff
fi

# Custom environment with isolated history.

_my.enable_env()
{
  export MY_ENV="$1"
  export HISTFILE="$2.bash_history"
  # Clear old history and read the new history from $HISTFILE
  history -cr

  cd $(cat $2)
  alias cdp="cd $(pwd)"
  alias e="emacsclient -a= -c -s $1"
  alias e.kill="emacsclient -a= -s $1 -e '(save-buffers-kill-emacs)'"

  # most of the projects are meson projects, so blindly run ninja
  alias b="(cdp; cd build && ninja)"
  alias r="(cdp; cd build && ninja && meson devenv $1)"
  alias g="(cdp; cd build && ninja && meson devenv gdb $1)"

  # Set title
  PS1="$PS1\[\e]2;($1)\a\]"
}

# $1 = environment name
# $2 (optional) = the default path for the given environment
# Create a new environment with given name and cd to the given
# directory if any.  Also, create a separate bash_history file
# for the given environment.
v()
{
  local env_dir env_file

  if [[ $1 = "" ]]; then
    if [[ "$MY_ENV" ]]; then
      echo -e "'${PS_BOLD}${PS_GREEN}$MY_ENV${PS_RESET}' environment active"
    else
      echo "Empty environment name given"
    fi
    return
  fi

  if [[ "$1" =~ [^a-z-] ]]; then
    echo "Environment variables should have [a-z-] characters only"
    return
  fi

  env_file=~/.bash/my-env/$1
  env_dir=$2

  if [[ "$env_dir" ]] && [[ ! -d "$env_dir" ]]; then
    echo "'$env_dir' is not a directory"
    return
  fi

  if [[ ! "$env_dir" && -r "$env_file" ]]; then
    env_dir=$(cat $env_file)
  fi

  if [[ ! "$env_dir" ]]; then
    env_dir=$(pwd)
  fi

  if [[ $env_dir == "$HOME" ]]; then
     echo "directory is "'$'"HOME', not setting env"
     return
  fi

  touch $env_file

  # If the directory is in a project subdirectory
  # create a symlink
  if [[ ! -e "$env_dir" ]]; then
    if [[ $(dirname $env_dir) = "$HOME/Projects" ]]; then
      if [[ -d "$HOME/Projects/personal-projects/$(basename $env_dir)" ]]; then
        ln -s "$HOME/Projects/personal-projects/$(basename $env_dir)" $env_dir
      elif [[ -d "$HOME/Projects/work-projects/$(basename $env_dir)" ]]; then
        ln -s "$HOME/Projects/work-projects/$(basename $env_dir)" $env_dir
      elif [[ -d "$HOME/Projects/archive-projects/$(basename $env_dir)" ]]; then
        ln -s "$HOME/Projects/archive-projects/$(basename $env_dir)" $env_dir
      fi
    fi
  fi

  (cd "$env_dir"; pwd > $env_file)

  # Save current history
  history -a

  # Load a subshell with new history
  bash --rcfile <(echo "source ~/.bashrc; _my.enable_env $1 $env_file")
}

_v()
{
  local cur prev options

  # Array variable storing the possible completions.
  COMPREPLY=()
  CURRENT=${COMP_WORDS[COMP_CWORD]}
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"
  options="$(cd ~/.bash/my-env; ls | grep -v bash_history)"

  if [[ ${#COMP_WORDS[@]} -le 2 ]]; then
    COMPREPLY=($(compgen -W "${options}" -- $cur))
  elif [[ ${#COMP_WORDS[@]} -le 3 ]]; then
    _filedir
  fi

  return 0
}

mkdir -p ~/.bash/my-env
complete -F _v v

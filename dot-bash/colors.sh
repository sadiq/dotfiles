# -*- mode: shell-script; -*-

if [ "$(type -P dircolors 2>/dev/null)" ]; then
  # Don't test for the file so that it fails with error message if file is missing
  eval "$(dircolors -b ~/.dir_colors)"
fi

# Color documentation (not complete):
# color chart: https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg
# 38;5;color -> fg color
# 48;5;color -> bg color
# 01;         -> bold
# 04;         -> underline
# 05;         -> blink

RESET="0"
BOLD="01"
RED="38;5;196"
GREEN="38;5;35"
BLUE="38;5;33"
LRED="38;5;203"
PINK="38;5;205"

PS_RESET="\e[0m"
PS_BOLD="\e[${BOLD}m"
PS_RED="\e[${RED}m"
PS_GREEN="\e[${GREEN}m"
PS_BLUE="\e[${BLUE}m"
PS_LRED="\e[${LRED}m"
PS_PINK="\e[${PINK}m"


# See https://gcc.gnu.org/onlinedocs/gcc/Diagnostic-Message-Formatting-Options.html
export GCC_COLORS="error=$BOLD;$RED"

# Colored man pages
export LESS_TERMCAP_md=$(printf "${PS_BOLD}${PS_LRED}")
export LESS_TERMCAP_me=$(printf "$PS_RESET")
export LESS_TERMCAP_us=$(printf "${PS_BOLD}${PS_GREEN}")
export LESS_TERMCAP_ue=$(printf "$PS_RESET")


#!/usr/bin/env python3

import os
from pathlib import Path
from typing import Optional, TypeVar

Settings = TypeVar("Settings")
try:
    from gi.repository import Gio
except:
    HAS_GIO = False
else:
    HAS_GIO = True


# Also update install.py
symlinks = {
    ".bashrc": Path(".").resolve() / "dot-bash/bashrc.sh",
    ".jhbuild": Path("/media/sadiq/main/jhbuild/install/"),
    ".cache/jhbuild": Path("/media/sadiq/main/.cache/jhbuild/"),
    ".cache/buildstream": Path("/media/sadiq/main/.cache/buildstream/"),
    ".cache/ccache": Path("/media/sadiq/main/.cache/ccache/"),
    ".config/emacs/init": Path(".").resolve() / "dot-config/emacs/init",
    "Projects": Path("/media/sadiq/main/jhbuild/checkout/"),
}


def remove_symlinks() -> None:
    conf_files = list(
        f
        for f in Path(".").rglob("*")
        if str(f).startswith("dot-") and not "personal" in str(f)
    )

    for f in conf_files:
        f_home = Path.home() / Path(str(f).replace("dot-", "."))
        f_abs = f.resolve()

        if f_home.is_symlink() and f_home.samefile(f_abs):
            f_home.unlink()
            print(f"Removed symlink: {f_home}")

    for link, target in symlinks.items():
        f_link = Path.home() / link

        if f_link.is_symlink() and f_link.samefile(target):
            f_link.unlink()
            print(f"Removed symlink: {f_link}")


def get_settings(schema_id: str, path: Optional[str] = None) -> Optional[Settings]:
    source = Gio.SettingsSchemaSource
    if not source.get_default() or not source.lookup(
        source.get_default(), schema_id, True
    ):
        print(f"Schema '{schema_id}' not found")
        return None
    if path:
        return Gio.Settings.new_with_path(schema_id, path)
    return Gio.Settings.new(schema_id)


# Reset only the most invasive settings
def reset_key_gsettings() -> None:
    if settings := get_settings("org.gnome.desktop.interface"):
        if settings.get_string("gtk-key-theme") == "Emacs":
            print("Reset: org.gnome.desktop.interface gtk-key-theme")
            settings.reset("gtk-key-theme")


def main() -> None:
    remove_symlinks()
    if HAS_GIO:
        reset_key_gsettings()

    if Path('/etc/keyd/default.conf').exists():
        print("Uninstall '/etc/keyd/default.conf' manually")

if __name__ == "__main__":
    main()
else:
    raise ImportError("uninstall.py can't be imported")

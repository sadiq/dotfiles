#!/usr/bin/env sh

# This scripts installs packages and change
# some configurations.

# List of packages to be installed
# Variables without _FULL suffix are base packages
# that I install for myself and others.  Packages
# in *_FULL are packages that I personally use

# Package names common in Alpine, Arch, Debian and Fedora
# shell
COMMON="bash bash-completion"
COMMON="$COMMON gnome-console gnome-terminal"

# utils
COMMON="$COMMON bat coreutils curl mpv htop tree w3m wget"
COMMON="$COMMON light playerctl wireplumber"
COMMON="$COMMON git neovim rsync whois"
COMMON="$COMMON gedit gitg gnome-text-editor"
COMMON="$COMMON gnome-tweaks nautilus totem"
# UI
COMMON="$COMMON sway waybar weston"

# dev
COMMON_FULL="nodejs"
COMMON_FULL="$COMMON_FULL ccache sccache meson mold"
COMMON_FULL="$COMMON_FULL gdb sysprof valgrind"
COMMON_FULL="$COMMON_FULL devhelp sqlitebrowser colordiff"
COMMON_FULL="$COMMON_FULL gnome-builder"
# utils
COMMON_FULL="$COMMON_FULL audacity chromium easytag mumble"
COMMON_FULL="$COMMON_FULL flatpak jq nmap"


# dev
ALPINE_FULL="build-base ccache gdb meson mold ninja delta"
# utils
ALPINE_FULL="$ALPINE_FULL openssh utmps"
# build-dep
ALPINE_FULL="$ALPINE_FULL accountsservice-dev krb5-dev libpwquality-dev"
ALPINE_FULL="$ALPINE_FULL libsecret-dev olm-dev"
ALPINE_FULL="$ALPINE_FULL libadwaita-dev libhandy1-dev"
ALPINE_FULL="$ALPINE_FULL libsoup-dev gspell-dev ncurses-dev"
ALPINE_FULL="$ALPINE_FULL modemmanager-dev gnome-desktop-dev libphonenumber-dev"
ALPINE_FULL="$ALPINE_FULL feedbackd-dev gsound-dev pipewire-dev pulseaudio-dev"
ALPINE_FULL="$ALPINE_FULL gnome-bluetooth-dev libgtop-dev samba-dev udisks2-dev"
ALPINE_FULL="$ALPINE_FULL evolution-dev folks-dev pidgin-dev"
ALPINE_FULL="$ALPINE_FULL colord-dev colord-gtk-dev"
# completion
ALPINE_FULL="$ALPINE_FULL git-bash-completion meson-bash-completion"
# fonts
ALPINE_FULL="$ALPINE_FULL font-noto-all"


# dev
DEBIAN="build-essential python3-pip ipython3"
# utils
DEBIAN="$DEBIAN apt-file fastboot gtypist"
# todo: add 'scrcpy' once in debian testing/stable
DEBIAN="$DEBIAN mpv-mpris vlc yt-dlp"
# fonts
DEBIAN="$DEBIAN fonts-inconsolata fonts-smc"

# dev
DEBIAN_FULL="golang gopls clangd manpages-dev global arduino gcovr pmbootstrap thonny git-delta"
DEBIAN_FULL="$DEBIAN_FULL python3-pylsp-rope python3-pylsp-black python3-pylsp-mypy shellcheck"
# docs
DEBIAN_FULL="$DEBIAN_FULL libgtk-3-doc libgtk-4-doc libadwaita-1-doc libgnome-bluetooth-doc"
DEBIAN_FULL="$DEBIAN_FULL libgdk-pixbuf2.0-doc libglib2.0-doc libpango1.0-doc"
DEBIAN_FULL="$DEBIAN_FULL libgnome-desktop-doc libgoa-1.0-doc libgraphene-doc"
DEBIAN_FULL="$DEBIAN_FULL libjson-glib-doc libgcr-3-doc libsoup-3.0-doc libsoup2.4-doc"
# utils
DEBIAN_FULL="$DEBIAN_FULL sqlite3 xvfb visidata"
DEBIAN_FULL="$DEBIAN_FULL artha wireshark docker.io virt-manager"


# shell
TERMUX="bash bash-completion"
# dev
TERMUX="$TERMUX build-essential python"
# utils
TERMUX="$TERMUX bat coreutils curl git git-delta gtypist htop"
TERMUX="$TERMUX neovim w3m tree wget unzip zip"
TERMUX_FULL="emacs gdb mold openssh rust valgrind"

# Used only by termux
PIP="yt-dlp"
PIP_FULL="gcovr pylsp-mypy visidata"


# We care only recent distributions that has
# /etc/os-release
get_distro()
{
  if [ ! -e "/etc/os-release" ]; then
    if type termux-setup-storage >/dev/null 2>&1; then
      echo "termux"
    else
      echo "unknown distro: not supported"
    fi

    return
  fi

  . /etc/os-release

  if [ "$ID" = "postmarketos" ] || [ "$ID_LIKE" = "alpine" ]; then
    echo "alpine"
  elif [ "$ID" = "debian" ] || [ "$ID_LIKE" = "debian" ]; then
    echo "debian"
  else
    echo "'$ID' not supported"
  fi
}

install_packages()
{
  DISTRO=$(get_distro)

  if [ -z "${DISTRO##*not supported*}" ]; then
    echo "$DISTRO"
    exit 1
  fi

  if [ -e "/etc/os-release" ]; then
    . /etc/os-release
  fi

  if [ "$DISTRO" = "debian" ]; then
    apt update || exit
    sudo apt-get build-dep -y gnome-control-center bijiben || exit 1
    INSTALL_CMD="apt-get install -y"
    OS=DEBIAN
  elif [ "$DISTRO" = "termux" ]; then
    apt update || exit
    INSTALL_CMD="apt install -y"
    OS=TERMUX
  elif [ "$DISTRO" = "alpine" ]; then
    apk update || exit
    INSTALL_CMD="apk add"
    OS=ALPINE
  fi

  # Ignore COMMON_FULL packages for alpine
  if [ "$DISTRO" = "alpine" ]; then
    COMMON_FULL=
  fi

  # Ignore COMMON and COMMON_FULL packages for termux
  if [ "$DISTRO" = "termux" ]; then
    COMMON=
    COMMON_FULL=
  fi

  # Populate package names
  if [ "$1" = "full" ]; then
    PACKAGES=$(eval echo "$COMMON $COMMON_FULL \$$OS \$${OS}_FULL")
    PIP="$PIP $(eval echo "\$PIP_FULL")"
  else
    PACKAGES=$(eval echo "$COMMON \$$OS")
  fi

  $INSTALL_CMD $PACKAGES || exit

  if [ "$DISTRO" = "alpine" ]; then
    # change shell for postmarketos
    if id "user" &>/dev/null; then
      sudo -u user chsh -s /bin/bash
    fi
    # fixme: Not working as root: PAM: Authentication failure
    # chsh -s /bin/bash

    setup-sshd openssh
    setup-utmp
  fi

  # todo: adapt to pep-0668
  # pip3 --break-system-packages
  # Install pip packages
  if [ "$DISTRO" = "termux" ]; then
    pip3 install $PIP || exit
  fi
}

if [ ! "$1" ]; then
  echo "Run as '$0 full' for complete installation"
elif [ "$1" != "full" ]; then
  echo "'$1' not supported.  Run as '$0 full' for complete installation"
  exit 1
fi

install_packages $1

echo "you may add to fstab:"
echo " LABEL=main /media/sadiq/main auto nosuid,dev,exec,nofail,x-gvfs-show 0 0"
# Remind pending things to be done
echo "todo:"
echo " jhbuild: https://wiki.gnome.org/Projects/Jhbuild/Introduction"
# noto font is too large, so not installed by default
echo " apt-get install fonts-noto"

# doc: man 5 sway

# Super key
set $mod Mod4

# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l

# Termianl
set $term gnome-terminal
# Application launcher
set $menu dmenu_path | dmenu | xargs swaymsg exec --

# Keybindings
# Using --to-code so that it works regardless of the layout used
bindsym --to-code {
    # Reload the configuration file
    $mod+Shift+c reload

    # Super + Enter -> open terminal
    $mod+Return exec $term
    # Start your launcher
    XF86Search exec $menu
    $mod+d exec $menu

    # Switch to workspace
    $mod+1 workspace number 1
    $mod+2 workspace number 2
    $mod+3 workspace number 3
    $mod+4 workspace number 4
    $mod+5 workspace number 5
    $mod+6 workspace number 6
    $mod+7 workspace number 7

    # Exit sway (logs you out of your Wayland session)
    $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'

    # Make the current focus fullscreen
    $mod+f fullscreen

    # Screen brightness
    XF86MonBrightnessUp exec light -A 10    # increase
    XF86MonBrightnessDown exec light -U 10  # decrease

    # Media controls
    XF86AudioPlay --locked exec playerctl play-pause
    XF86AudioNext --locked exec playerctl next
    XF86AudioPrev --locked exec playerctl previous

    # Volume controls
    XF86AudioRaiseVolume --locked exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.05+ -l 1.0
    XF86AudioLowerVolume --locked exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.05- -l 1.0
    XF86AudioMute --locked exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
}

;;; common.el --- Common configurations -*- lexical-binding: t; -*-

;; Configurations that are common to more than one Emacs mode.
;; Or configurations that doesn't fit anywhere else.  Completion
;; related settings are in `completion.el'.

(use-package avy
  :bind ("M-g M-g" . avy-goto-line)
  :custom
  ;; Ignore case on avy search
  (avy-case-fold-search nil))

(use-package dwim-coder-mode
  :ensure nil
  :hook ((c-ts-mode python-ts-mode rust-ts-mode emacs-lisp-mode) . dwim-coder-mode))

(use-package diff-hl
  :config
  ;; mark diff even if not saved
  (diff-hl-flydiff-mode)
  (global-diff-hl-mode))

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs
               '((c-mode c++-mode c-ts-mode) . ("clangd")))
  :hook
  ((c-mode c-ts-mode python-mode go-ts-mode) . eglot-ensure)
  ((eglot-managed-mode) . (lambda ()
                            (setq eldoc-documentation-strategy #'eldoc-documentation-compose)
                            (eglot-inlay-hints-mode -1))))

(use-package magit
  :init
  ;; See https://github.com/jwiegley/use-package/issues/965
  (add-hook 'git-rebase-mode-hook 'my/git-rebase-mode-goto-end 91))

(use-package puni
  :bind ([remap puni-kill-region] . my/kill-region-or-backward-word)
  :hook ((prog-mode nxml-mode) . puni-mode))

(use-package marginalia
  :init
  (marginalia-mode))

(use-package ws-butler
  :hook (prog-mode text-mode conf-mode))

(use-package electric-operator
  :hook (c-mode c-ts-mode python-mode python-ts-mode rust-ts-mode))

(use-package yasnippet
  :custom
  (yas-snippet-dirs `(,(expand-file-name "init/yasnippets" user-emacs-directory)))
  :config
  (yas-global-mode 1))

(use-package eldoc-box
  :preface
  ;; Adapted from `eldoc-box--default-at-point-position-function-1'
  (defun my/eldoc-box-at-point-position (width height)
    (let* ((point-pos (eldoc-box--point-position-relative-to-native-frame))
           ;; calculate point coordinate relative to native frame
           ;; because childframe coordinate is relative to native frame
           (x (car point-pos))
           (y (cdr point-pos)))
      (cons
       ;; Try keeping the box at the right with an offset of 10 pixel
       ;; fixme: Why do I need to offset `frame-inner-width' by 10 pixels?
       (max 0 (min (+ x 10) (- (frame-inner-width) width 10)))
       ;; Try keeping the box at the top with an offset of 15 pixel
       ;; Also add a smaller offset of 5 if the box is too small
       (max 0 (- y height (if (<= height (* 3 (frame-char-height))) 5 15))))))

  :init
  (setq eldoc-box-at-point-position-function #'my/eldoc-box-at-point-position)
  :hook ((eldoc-mode) . eldoc-box-hover-at-point-mode))

(use-package meson-mode)
(use-package xr)

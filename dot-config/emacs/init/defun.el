;;; defun.el --- Personal defuns -*- lexical-binding: t; -*-

(require 'dwim-coder-mode)

(defun my/c-open-other-file ()
  (interactive)
  (let ((file-name (file-name-sans-extension buffer-file-name))
        (other-file buffer-file-name))

    (cond
     ((string-suffix-p ".c" buffer-file-name)
      ;; If there is file-private.h use that, other wise use file.h
      (setq other-file (concat file-name "-private.h"))
      (unless (file-exists-p other-file)
        (setq other-file (concat file-name ".h"))))
     ((string-suffix-p "-private.h" buffer-file-name)
      (setq other-file (concat (string-remove-suffix "-private" file-name) ".h"))
      (unless (file-exists-p other-file)
        (setq other-file (concat (string-remove-suffix "-private" file-name) ".c"))))
     ((string-suffix-p ".h" buffer-file-name)
      (setq other-file (concat file-name ".c")))
     (t
      (cl-assert nil)))
    (find-file other-file)
    ))

(defun my/beg-or-end-of-buffer ()
  (interactive)
  (if (eq (point) (point-max))
      (goto-char (point-min))
    (goto-char (point-max))))

(defun my/switch-buffer ()
  (interactive)
  (if (one-window-p)
      (consult-buffer)
    (other-window 1)))

(defun my/toggle-hiding ()
  (interactive)
    (cond
     ((eq major-mode 'web-mode)
      (web-mode-fold-or-unfold))
     ((eq major-mode 'org-mode)
      (org-cycle))
     (t
      (hs-toggle-hiding))))

(defun my/kill-region-or-backward-word ()
  (interactive)
  (call-interactively
   ;; Act on currently marked region, if any
   (if (use-region-p)
       'kill-region
     'backward-kill-word)))

;; todo
(defun my/kill-line (&optional n)
  (interactive "P")
  (let* (start (point) (end nil)
               (line-num (line-number-at-pos (point))))
    (if (not (bound-and-true-p puni-mode))
        (kill-line n)
      (if n
          (puni-kill-line n)
        (puni-soft-delete-by-move
         ;; FUNC: `puni-soft-delete-by-move` softly deletes the region from
         ;; cursor to the position after calling FUNC.
         (lambda ()
           (if (eolp) (forward-char) (puni-end-of-sexp)))
         ;; STRICT-SEXP: More on this later.
         'strict-sexp
         ;; STYLE: More on this later.
         'within
         ;; KILL: Save deleted region to kill-ring if non-nil.
         'kill
         ;; FAIL-ACTION argument is not used here.
         'delete-one
         ))
    )))

;; Goto the beginning of the last non-comment
;; line in the commit list
(defun my/git-rebase-mode-goto-end ()
  (interactive)
  (end-of-buffer)
  (beginning-of-line)
  (while (and (not (bobp))
            (looking-at-p "\\(^#\\|^$\\)"))
    (previous-line)
    (beginning-of-line)))

(defun my/term ()
  (interactive)
  ;; if already on term, switch to previously opened buffer
  (if (memq major-mode '(term-mode vterm-mode eshell-mode))
      (if (> (count-windows) 1)
          (delete-window)
        (previous-buffer))
    (if (fboundp 'vterm-toggle-cd)
        (vterm-toggle-cd)
      (let ((default-directory (file-name-directory
                                (or buffer-file-name default-directory))))
            (eshell)))))

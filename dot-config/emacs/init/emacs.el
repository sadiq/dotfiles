;;; emacs.el --- Emacs and builtin mode configurations -*- lexical-binding: t; -*-

(use-package emacs
  :bind
  ("C-h" . delete-backward-char)
  ("C-w" . my/kill-region-or-backward-word)
  ("<f5>" . my/term)
  ("<f8>" . devhelp-word-at-point)

  :preface
  (defun my/buffer ()
    (if (minibufferp)
        (window-buffer (minibuffer-selected-window))
      (current-buffer)))

  :custom
  (custom-file "~/.config/emacs/custom.el")
  (use-short-answers t "Change 'yes or no' to 'y or n'")
  (completion-cycle-threshold 3)
  ;; todo look into define-fringe-bitmap in ui.el
  (flymake-fringe-indicator-position nil)
  ;; use controlmaster config from ~/.ssh/config
  (tramp-use-ssh-controlmaster-options nil)
  (gdb-many-windows t)

  ;; Show buffer name as title and a bullet if unsaved
  (frame-title-format
   '((:eval (buffer-name (my/buffer)))
     " - Emacs "
     (:eval (if (buffer-modified-p (my/buffer)) "⦁" "  "))))
  (completion-ignore-case t)

  ;; Indentation
  (tab-always-indent 'complete)
  (indent-tabs-mode nil)
  (standard-indent 2)

  :init
  (load "/usr/share/doc/devhelp/devhelp.el" t)
  (load "/etc/emacs/site-start.d/50devhelp.el" t)
  (load custom-file t)

  (prefer-coding-system 'utf-8)
  ;; Highlight matching parenthesis
  (show-paren-mode 1)
  ;; Show column number -- The number of characters from the beginning
  ;; of the current line up to the cursor
  (column-number-mode 1)
  ;; Save and restore point location for each visited file
  (save-place-mode 1)
  ;; Auto close quotes and brackets: ", [, (, {, etc.
  (electric-pair-mode 1)
  ;; Remember window change configuration
  (winner-mode 1)

  :hook
  ((prog-mode conf-mode text-mode) . (lambda ()
                                       (setq-local show-trailing-whitespace t)
                                       (setq-local require-final-newline t)))
  (prog-mode . hs-minor-mode))

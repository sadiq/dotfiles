;;; package.el --- Manage packages -*- lexical-binding: t; -*-

(use-package package
  :custom
  (package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa-stable" . "https://stable.melpa.org/packages/")
     ("melpa" . "https://melpa.org/packages/")))
  (package-archive-priorities
   '(("gnu" . 30)
     ("melpa-stable" . 20)
     ("melpa" . 10))))

(use-package use-package
  :after package
  :custom
  (use-package-always-ensure t))

(use-package keypression
  :custom
  (keypression-cast-command-name nil)
  (keypression-use-child-frame nil)
  (keypression-fade-out-delay 1.0)
  (keypression-cast-command-name t)
  (keypression-background-for-dark-mode "black")
  (keypression-foreground-for-dark-mode "white")
  (keypression-cast-command-name-format "%s")
  (keypression-combine-same-keystrokes t)
  (keypression-font-face-attribute '(:width normal :height 300 :weight bold)))

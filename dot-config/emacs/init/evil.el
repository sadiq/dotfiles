;;; evil.el --- `evil-mode' Configurations -*- lexical-binding: t; -*-

;; Keys not specific to `evil-mode' are set in `keys.el'

(use-package evil
  :init
  (setq evil-disable-insert-state-bindings t)
  (setq evil-want-keybinding nil)

  :custom
  (evil-cross-lines t)
  (evil-want-fine-undo t)
  (evil-move-cursor-back nil)
  (evil-move-beyond-eol t)

  :config
  (add-to-list 'evil-buffer-regexps '("^\\*vc-log\\*$" . insert))
  (add-to-list 'evil-insert-state-modes 'vterm-mode)
  (evil-mode 1)

  (evil-define-key '(insert visual) 'global
    (kbd "<f5>") 'my/term)

  (evil-define-key '(normal visual) 'global
    (kbd "<f5>") 'my/term
    (kbd "t") 'my/toggle-hiding
    (kbd "fd") 'xref-find-definitions-other-window
    (kbd "ff") 'xref-find-references
    (kbd "u") 'undo
    (kbd "SPC") 'scroll-up
    (kbd "S-SPC") 'scroll-down
    (kbd "\C-e") 'move-end-of-line)

  (evil-define-key 'normal 'global
    (kbd "gg") 'my/beg-or-end-of-buffer
    (kbd "g SPC") 'avy-goto-char-2
    (kbd "fj") 'avy-goto-line
    (kbd "fl") 'find-file
    (kbd "fk") 'project-find-file
    (kbd "fm") 'consult-flymake
    (kbd "p") 'evil-paste-before
    (kbd "s") 'save-buffer
    (kbd "n") 'my/c-open-other-file
    (kbd ";") 'my/switch-buffer
    (kbd "f;") 'consult-buffer

    ;; vcs/diff-hl keybindings
    (kbd "f SPC") 'vc-next-action
    (kbd "<") 'diff-hl-previous-hunk
    (kbd ">") 'diff-hl-next-hunk
    (kbd "C-M") 'diff-hl-revert-hunk)

  (evil-define-key 'visual 'global
    (kbd ";") 'comment-dwim
    (kbd "gg") 'keyboard-quit)

  (evil-define-key 'normal org-mode-map "ff" 'org-todo))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

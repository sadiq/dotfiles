;;; ui.el --- [G]UI configurations -*- lexical-binding: t; -*-

;; Configurations that relates to Emacs UI elements in general.
;; Some of those are set in `early-init.el' and some in `emacs.el'

(use-package modus-themes
  :pin melpa-stable
  :custom
  (modus-vivendi-palette-overrides '((fg-main "#f5f5f5")))
  (modus-themes-headings '((t . (medium))))
  :custom-face
  ;; hint: M-x describe-face
  (web-mode-html-attr-value-face ((t (:foreground "#ef9a9a"))))
  (font-lock-string-face ((t (:foreground "#ef9a9a"))))
  (avy-lead-face ((t (:inherit modus-themes-reset-soft))))
  (avy-lead-face-0 ((t (:inherit modus-themes-reset-soft))))
  (avy-lead-face-2 ((t (:inherit modus-themes-reset-soft))))
  (diff-hl-change ((t (:background "#00afff" :foreground "#00afff"))))
  (diff-hl-delete ((t (:background "#ff005f" :foreground "#ff005f"))))
  (diff-hl-insert ((t (:background "#00af00" :foreground "#00af00"))))
  (web-mode-current-element-highlight-face ((t (:background "#333333"))))
  :config
  (load-theme 'modus-vivendi 1)
  (modus-themes-with-colors
    (custom-set-faces
     `(modus-themes-lang-error   ((,c :foreground ,red-cooler :background ,bg-red-nuanced
                                      :underline (:style line :color ,underline-err :position 0))))
     `(modus-themes-lang-warning ((,c :foreground ,yellow-cooler :background ,bg-yellow-nuanced
                                      :underline (:style line :color ,underline-warning :position 0))))
     `(modus-themes-lang-note    ((,c :foreground ,blue-cooler :background ,bg-blue-nuanced
                                      :underline (:style line :color ,blue :position 0))))
     )))

;; Hide vertical window divider
(use-package frame
  :ensure nil
  :custom
  (window-divider-default-places 'right-only)
  (window-divider-default-right-width 1)
  (window-divider-mode 1)
  :custom-face
  (window-divider ((t (:foreground "black")))))

(use-package highlight-parentheses
   :config
   (setq hl-paren-background-colors '("#903232"))
   (setq hl-paren-colors '("#f5f5f5"))
   (global-highlight-parentheses-mode))

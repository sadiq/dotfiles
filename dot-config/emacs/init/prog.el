;;; prog.el --- Programming major mode Configurations -*- lexical-binding: t; -*-

;; If the configuration is large, it may be listed in a separate file, not here.

(add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
(add-to-list 'major-mode-remap-alist '(css-mode . css-ts-mode))
(add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))

;; C
;; hack: Never do this, Many projects enforce c-mode
;; via file variables. Override them
(defalias 'c-mode #'c-ts-mode)
(use-package c-ts-mode
  :ensure nil
  :after (dwim-coder-mode eglot)
  :custom
  (dwim-coder-c-sub-style 'gnome)
  :mode "\\.[ch]\\'")

;; CSS
(use-package css-mode
  :ensure nil
  :custom
  (css-indent-offset 2))

;; Golang
(use-package go-ts-mode
  :ensure nil
  :mode "\\.go\\'")

;; Python
(use-package python
  :ensure nil
  :after (dwim-coder-mode eglot)
  :mode ("\\.py\\'" . python-ts-mode))

;; Rust
(use-package rust-ts-mode
  :ensure nil
  :after (dwim-coder-mode)
  :mode "\\.rs\\'")

;; Shell script
(use-package bash-ts-mode
  :ensure nil
  :custom
  (sh-basic-offset 2)
  :mode "\\.sh\\'")

;; Web, XML/HTML
(use-package web-mode
  :config
  (setq web-mode-content-types-alist '(("xml"  . "\\.ui\\'")))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-enable-current-element-highlight t)
  :mode ("\\.html?\\'" "\\.ui\\'" "\\.xml\\'"))

(use-package csv-mode)

# -*- mode: snippet; require-final-newline: nil -*-
# name: Initialize the function, function class, etc
# key: f.
# condition: (string-suffix-p ".c" buffer-file-name)
# expand-env: ((yas-indent-line 'fixed))
# --
#define G_LOG_DOMAIN "${1:$(dwim-coder-s-to-style yas-text "lisp")}"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "`(file-name-sans-extension (file-name-nondirectory buffer-file-name))`.h"

struct _${1:$(dwim-coder-s-to-style yas-text "upcamel")}
{
  ${2:$(dwim-coder-s-to-style (replace-regexp-in-string "TYPE_" "" yas-text) "upcamel")}    parent_instance;
};


G_DEFINE_TYPE (${1:$(dwim-coder-s-to-style
                     yas-text "upcamel")}, ${1:`(dwim-coder-s-to-style
                     (file-name-sans-extension (file-name-nondirectory buffer-file-name))
                     "snake")`$(dwim-coder-s-to-style
                     yas-text "snake")}, ${2:G_TYPE_OBJECT$(dwim-coder-s-to-style (downcase yas-text) "upsnake")})

static void
$1_finalize (GObject *object)
{
  ${1:$(dwim-coder-s-to-style yas-text "upcamel")} *self = (${1:$(dwim-coder-s-to-style yas-text "upcamel")} *)object;

  G_OBJECT_CLASS ($1_parent_class)->finalize (object);
}

static void
$1_class_init (${1:$(dwim-coder-s-to-style yas-text "upcamel")}Class *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
${2:$(if (string-match-p "GTK\\\|ADW" yas-text) "  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);\n")}
  object_class->finalize = $1_finalize;
}

static void
$1_init (${1:$(dwim-coder-s-to-style yas-text "upcamel")} *self)
{
${2:$(if (string-match-p "GTK\\\|ADW" yas-text) "  gtk_widget_init_template (GTK_WIDGET (self));")}
}

${5:${1:$(dwim-coder-s-to-style yas-text "upcamel")}$(dwim-coder-s-to-style yas-text "upcamel")} *
$1_new (void)
{
  return g_object_new (${1:$(replace-regexp-in-string "\\\\(^[A-Z]*_\\\\)\\\\(.*\\\\)" "\\\1TYPE_\\\2" (upcase yas-text))}, NULL);
}

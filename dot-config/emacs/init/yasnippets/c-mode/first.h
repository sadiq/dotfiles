# -*- mode: snippet; require-final-newline: nil -*-
# name : GObject declare
# key: f.
# condition: (string-suffix-p ".h" buffer-file-name)
# expand-env: ((yas-indent-line 'fixed))
# --
#pragma once

#include <${2:$(if (string-match-p "gtk" yas-text) "gtk/gtk"
                     (if (string-match-p "adw" yas-text) "adwaita"
                        "glib-object"))}.h>

G_BEGIN_DECLS

#define ${1:$(replace-regexp-in-string "\\\\(^[A-Z]*_\\\\)\\\\(.*\\\\)"
                                       "\\\1TYPE_\\\2" (upcase yas-text))} (${1:`(dwim-coder-s-to-style
                (file-name-sans-extension (file-name-nondirectory buffer-file-name))
                "snake")`$(dwim-coder-s-to-style yas-text "snake")}_get_type ())
G_DECLARE_FINAL_TYPE (${1:$(dwim-coder-s-to-style
                       yas-text "upcamel")}, $1, ${1:$(replace-regexp-in-string "\\\\(^[A-Z]*\\\\).*"
                       "\\\1" (upcase yas-text))}, ${1:$(replace-regexp-in-string "^[A-Z]*_\\\\(.*$\\\\)"
                       "\\\1" (upcase yas-text))}, ${2:GObject$(dwim-coder-s-to-style
                       yas-text "upcamel")})

${1:$(dwim-coder-s-to-style
            yas-text "upcamel")} *$1_new (void);$0

G_END_DECLS

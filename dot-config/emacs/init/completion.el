;;; completion.el --- Completion related configurations -*- lexical-binding: t; -*-

;; Configurations related to various completions.  built-in
;; configuration changes are set in `emacs.el'

(use-package vertico
  :init
  (vertico-mode 1))

(use-package vertico-directory
  :after vertico
  :ensure nil
  :preface
  (defun my/vertico-directory-enter ()
    (interactive)
    (if (eq 'file (vertico--metadata-get 'category))
        (if (or (looking-back "[~/:\\]$" (line-beginning-position))
                ;; if after strings like . or ..
                (looking-back "/[.]*$" (line-beginning-position))
                ;; if the search line is empty
                (null (thing-at-point 'line))
                ;; if we have 0 completions
                (= vertico--total 0)
                ;; if first completion match is like ssh:, adb:, etc.
                (string-match-p "^[/]?[a-z]*:$" (vertico--candidate)))
            (insert "/")
          (vertico-directory-enter))
      (insert "/")))

  :bind (:map vertico-map
              ("/" . my/vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("C-h" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package consult
  :custom
  (consult-async-min-input 1)
  ;; (consult-buffer-filter '("\\` " "\\`\\*.*\\*\\'"))
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (([remap Info-search] . consult-info)
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("M-g f" . consult-flymake)
         ("M-g i" . consult-imenu-multi)
         ("M-s d" . consult-find)
         ("C-s" . consult-line-multi)
         :map minibuffer-local-map
         ("C-s" . consult-history))
  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)
  ;; The :init configuration is always executed (Not lazy)
  :init
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config
  ;; todo: Hide ^[*].*[*]$
  ;; and always search in current buffer, regardless of whether
  ;; it is hidden or not
  ;; also, list hidden bufffer in buffer switcher if no normal buffers
  ;; are visible
  (add-to-list 'consult-buffer-filter "^[*]EGLOT.*[*]$")
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key '(:debounce 0.4 any)))

(use-package corfu
  :custom
  (corfu-auto t)                 ;; Enable auto completion
  (corfu-auto-prefix 1)
  (corfu-min-width 35)
  (corfu-popupinfo-delay 0)
  :custom-face
  (corfu-popupinfo ((t (:height 1.1))))
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode))

(use-package orderless
  :init
  (setq completion-styles '(basic orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

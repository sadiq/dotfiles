;;; early-init.el --- Emacs init -*- lexical-binding: t; -*-

;; Helper script to load other files
;; This script just loads other files. Nothing else. KISS?

;; We require GNU Emacs 29.1+
(unless (>= emacs-major-version 29)
  (error "GNU Emacs 29.1+ Required"))

(when (file-directory-p "~/Projects/personal-projects/dwim-coder-mode")
  (add-to-list 'load-path "~/Projects/personal-projects/dwim-coder-mode"))

(when (file-directory-p "~/Projects/archive-projects/electric-operator")
  (load "~/Projects/archive-projects/electric-operator/electric-operator.el"))


;; Load files to be loaded in order
(load (concat (expand-file-name "init" user-emacs-directory) "/emacs"))
(load (concat (expand-file-name "init" user-emacs-directory) "/package"))
(load (concat (expand-file-name "init" user-emacs-directory) "/ui"))

;; Now load all files
(mapc 'load (directory-files
             (expand-file-name "init" user-emacs-directory) t "^[a-z0-9-]+[.]elc?$"))

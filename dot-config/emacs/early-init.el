;;; early-init.el --- Emacs early init -*- lexical-binding: t; -*-

;; Changes to be applied before the UI is initialized

;; Open Emacs maximized
(push '(fullscreen . maximized) initial-frame-alist)
(push '(fullscreen . maximized) default-frame-alist)

;; Don't show startup screen
(setq inhibit-startup-message t)

;; Set black bg early so that it won't flash a white window first
(set-face-attribute 'default nil :background "black" :foreground "#f5f5f5")

;; Fonts should already be installed before you can use it.
;; hint: fc-list lists all fonts
(push '(font . "Inconsolata-14") default-frame-alist)

;; Hide space consuming things from screen
(push '(left-fringe . 2) default-frame-alist)
(push '(right-fringe . 0) default-frame-alist)
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
(set-face-attribute 'mode-line nil :box nil)
(set-face-attribute 'mode-line-inactive nil :box nil)

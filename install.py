#!/usr/bin/env python3

import argparse
import os
import sys
from pathlib import Path
from typing import Optional, TypeVar

full_install: Optional[bool] = None
Settings = TypeVar("Settings")
try:
    from gi.repository import Gio
except:
    HAS_GIO = False
else:
    HAS_GIO = True


# Also update uninstall.py
symlinks = {
    "Projects": Path("/media/sadiq/main/jhbuild/checkout/"),
    ".bashrc": Path(".").resolve() / "dot-bash/bashrc.sh",
    ".jhbuild": Path("/media/sadiq/main/jhbuild/install/"),
    ".cache/jhbuild": Path("/media/sadiq/main/.cache/jhbuild/"),
    ".cache/buildstream": Path("/media/sadiq/main/.cache/buildstream/"),
    ".cache/ccache": Path("/media/sadiq/main/.cache/ccache/"),
    ".config/emacs/init": Path(".").resolve() / "dot-config/emacs/init",
}


def check_symlinks(unmodified: list[Path]) -> None:
    link = ".bashrc"
    if (Path.home() / link).resolve() != symlinks[link]:
        unmodified.append(Path.home() / link)

    link = ".config/emacs/init"
    if (Path.home() / link).resolve() != symlinks[link]:
        unmodified.append(Path.home() / link)

    if unmodified:
        print("The following files are \033[1mnot\033[0m updated:")
        for f in unmodified:
            print(f)

    if (Path.home() / ".config/emacs/init.el").exists():
        if (Path.home() / ".emacs").exists():
            print("Please remove '~/.emacs' file for the installed config to work")

        if (Path.home() / ".emacs.d").exists():
            print("Please remove '~/.emacs.d' file for the installed config to work")


def create_symlinks() -> None:
    unmodified = []
    conf_files = list(
        f
        for f in Path(".").rglob("*")
        if str(f).startswith("dot-") and not {"emacs/init", "~", "#"} & set(str(f))
    )

    for link, target in symlinks.items():
        f_link = Path.home() / link
        if "/" in link:
            f_link.parent.mkdir(parents=True, exist_ok=True)

        if not (f_link.exists() or f_link.is_symlink()) and target.exists():
            f_link.symlink_to(target)
            print(f"{f_link} -> {target}")

    for f in conf_files:
        f_home = Path.home() / Path(str(f).replace("dot-", "."))
        f_abs = f.resolve()

        # Don't install personal settings if not full install
        if "sadiq" in str(f) in str(f):
            if full_install is None:
                if os.name != 'posix' or os.getlogin() != "sadiq":
                    continue
            elif not full_install:
                continue

        if not (f_home.exists() or f_home.is_symlink()):
            if f.is_symlink() or f.is_file():
                f_home.symlink_to(f_abs)
                print(f"{f_home} -> {f_abs}")
            elif f.is_dir():
                f_home.mkdir(parents=True)
                print(f"created directory: {f_home}")
            else:
                print(f"Invalid path: {f}")

        if f.is_symlink() or f.is_file() and f_home.resolve() != f_abs:
            unmodified.append(f_home)

    check_symlinks(unmodified)


def get_settings(schema_id: str, path: Optional[str] = None) -> Optional[Settings]:
    source = Gio.SettingsSchemaSource
    if not source.get_default() or not source.lookup(
        source.get_default(), schema_id, True
    ):
        print(f"Schema '{schema_id}' not found")
        return None
    if path:
        return Gio.Settings.new_with_path(schema_id, path)
    return Gio.Settings.new(schema_id)


def update_key_gsettings() -> None:
    print("Updating Keybinding gsettings")

    # Change invasive keybindings only on full_install
    if full_install:
        # Use GNU Emacs keybindings
        if settings := get_settings("org.gnome.desktop.interface"):
            settings.set_string("gtk-key-theme", "Emacs")

    # Enable tap to click
    if settings := get_settings("org.gnome.desktop.peripherals.touchpad"):
        settings.set_boolean("tap-to-click", True)

    if settings := get_settings("org.gnome.desktop.wm.keybindings"):
        strv = settings.get_strv("switch-to-workspace-left")
        if not "<Super>j" in strv:
            settings.set_strv("switch-to-workspace-left", strv + ["<Super>j"])

        strv = settings.get_strv("switch-to-workspace-right")
        if not "<Super>k" in strv:
            settings.set_strv("switch-to-workspace-right", strv + ["<Super>k"])

    if settings := get_settings("org.gnome.settings-daemon.plugins.media-keys"):
        strv = settings.get_strv("custom-keybindings")
        has_key = False

        # If <Control><Alt>t not set as terminal shortcut add it
        for s in strv:
            settings = get_settings(
                "org.gnome.settings-daemon.plugins.media-keys.custom-keybinding", s
            )
            binding = settings.get_string("binding")
            if binding == "<Control><Alt>t":
                has_key = True
                break

        if not has_key:
            path = "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom99/"
            settings = get_settings(
                "org.gnome.settings-daemon.plugins.media-keys.custom-keybinding", path
            )
            settings.delay()
            settings.set_string("command", "gnome-terminal")
            settings.set_string("binding", "<Control><Alt>t")
            settings.set_string("name", "GNOME Terminal")
            settings.apply()

            if not path in strv:
                settings = get_settings("org.gnome.settings-daemon.plugins.media-keys")
                settings.set_strv("custom-keybindings", strv + [path])


def update_ui_gsettings() -> None:
    print("Updating UI gsettings")

    # Prefer dark theme
    if settings := get_settings("org.gnome.desktop.interface"):
        settings.set_string("gtk-theme", "Adwaita-dark")
        settings.set_string("color-scheme", "prefer-dark")
    if settings := get_settings("org.gnome.Terminal.Legacy.Settings"):
        settings.set_string("theme-variant", "dark")

    # Prefer darker style scheme for editors
    if settings := get_settings("org.gnome.builder.editor"):
        settings.set_string("style-scheme-name", "builder-dark")
    if settings := get_settings("org.gnome.gedit.preferences.editor"):
        settings.set_string("scheme", "solarized-dark")
    if settings := get_settings("org.gnome.TextEditor"):
        settings.set_string("style-scheme", "solarized-dark")

    # Use Inconsolata as monospace font
    font = "Inconsolata Medium 12"
    if settings := get_settings("org.gnome.desktop.interface"):
        settings.set_string("monospace-font-name", font)
    font = "Inconsolata Medium 14"
    if settings := get_settings("org.gnome.gedit.preferences.editor"):
        settings.set_boolean("use-default-font", False)
        settings.set_string("editor-font", font)
    if settings := get_settings("org.gnome.TextEditor"):
        settings.set_boolean("use-system-font", False)
        settings.set_string("custom-font", font)
    if settings := get_settings("org.gnome.builder.editor"):
        settings.set_string("font-name", font)
    if settings := get_settings("org.gnome.builder.terminal"):
        settings.set_string("font-name", font)


def update_other_gsettings() -> None:
    print("Updating other gsettings")
    if settings := get_settings("org.gnome.desktop.interface"):
        settings.set_boolean("clock-show-date", True)
        settings.set_boolean("show-battery-percentage", True)


def main() -> None:
    create_symlinks()
    if full_install:
        (Path.home() / ".bash/.default").unlink(missing_ok=True)
    else:
        (Path.home() / ".bash/.default").touch()

    if HAS_GIO:
        update_key_gsettings()
        update_ui_gsettings()
        update_other_gsettings()

    print("Install 'etc/keyd/default.conf' to '/etc/keyd' manually")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("full", nargs="?", choices=("full", "nofull"))
    args = parser.parse_args()
    if args.full:
        full_install = args.full == "full"
    else:
        print(f"Run as '{sys.argv[0]} full' for complete installation")
    main()
else:
    raise ImportError("install.py can't be imported")
